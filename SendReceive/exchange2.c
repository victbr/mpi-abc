#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    int a, b, size, rank;

    MPI_Init(&argc, &argv); // Initialize the MPI environment
    MPI_Comm_size(MPI_COMM_WORLD, &size); // Get the number of processes
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); // Get the rank of the process

    /// sendrecv
    if (rank == 0){
        a = -1;
        MPI_Sendrecv(&a, 1, MPI_INT, 1-rank, 0, &b, 1, MPI_INT, 1 - rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d exchanged token %d to token %d with process %d\n", rank, a, b, 1 - rank);
    } else if (rank == 1){
        a = 1;
        MPI_Sendrecv(&a, 1, MPI_INT, 1-rank, 0, &b, 1, MPI_INT, 1 - rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d exchanged token %d to token %d with process %d\n", rank, a, b, 1 - rank);
    }

    /// sendrecv_replace
    if (rank == 0){
        a = -1;
        MPI_Sendrecv_replace(&a, 1, MPI_INT, 1-rank, 0, 1 - rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d exchanged replace token %d to token %d with process %d\n", rank, a, a, 1 - rank);
    } else if (rank == 1){
        a = 1;
        MPI_Sendrecv_replace(&a, 1, MPI_INT, 1-rank, 0, 1 - rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d exchanged replace token %d to token %d with process %d\n", rank, a, a, 1 - rank);
    }


    MPI_Finalize(); // Finalize the MPI environment.
    return 0;
}