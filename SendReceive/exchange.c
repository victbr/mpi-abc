#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    int a, b, size, rank;

    MPI_Init(&argc, &argv); // Initialize the MPI environment
    MPI_Comm_size(MPI_COMM_WORLD, &size); // Get the number of processes
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); // Get the rank of the process

    if (rank == 0){
        a = -1;
        MPI_Send(&a, 1, MPI_INT, 1-rank, 0, MPI_COMM_WORLD);
        printf("Process %d sent %d to process %d\n", rank, a, 1 - rank);
        MPI_Recv(&b, 1, MPI_INT, 1 - rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d received %d from process %d\n", rank, b, 1 - rank);
    } else if (rank == 1){
        a = 1;
        MPI_Recv(&b, 1, MPI_INT, 1 - rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // it is ok to recv a and send b. 
        MPI_Send(&a, 1, MPI_INT, 1 - rank, 0, MPI_COMM_WORLD);
        printf("Process %d received %d from process %d\n", rank, b, 1 - rank);
    }

    MPI_Finalize(); // Finalize the MPI environment.
    return 0;
}