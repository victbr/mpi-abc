# Send and Receive


```c
MPI_Send(void* buf, int count, MPI_DataType datatype, int dest, int tag, MPI_Comm comm);
MPI_Recv(void* buf, int count, MPI_DataType datatype, int source, int tag, MPI_Comm comm, MPI_Status* status);
```

`count` is the length of message, `datatype` is the datatype of msgs, `buf` is the place where the data is or the place to receive the data, `source` and `dest` is the rank of the other thread in this **communicator**. 

`tag` is a tag of msg, didn't figure out what it really is. 

`MPI_DataType` supports some built-in type like `MPI_CHAR` means `signed char`， `MPI_INT` means `signed int`. 

plus, `MPI_BYTE` means 8 bits, and `MPI_PACKED` is used to pack discontinuous data.

```c
int MPI_Get_count(MPI_Status* status, MPI_Datatype datatype, int*count);
```

`MPI_Get_count` is used to get the length of message by `MPI_Status`.

## pingpong

process 0 send data to process 1, process 1 receive the data and send back to process 0, process 0 receive the data.

## status

process 0 get the `MPI_Status` when receiving data from process 1. It has `MPI_SOURCE` and `MPI_TAG` and `MPI_ERROR`, the `MPI_ERROR` value is weird.

## exchange

exchange data between 2 processors.

## exchange2

```c++
MPI_Sendrecv(void* sendbuf, int sendcount, MPI_Datatype senddatatype, int dest, 
    int sendtag, void* recvbuf, int recvcount, MPI_Datatype recvdatatype, 
    int source, int recvtag, MPI_Comm comm, MPI_Status* status);

MPI_Sendrecv_replace(void* buf, int count, MPI_Datatype datatype, int dest, 
    int sendtag, int source, int recvtag, MPI_Comm comm, MPI_Status* status);
```

do send and recv in one function. replace means send and receive at the same place.

first send, and then recv. 