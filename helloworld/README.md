# Hello World!

example of how to write the simplest MPI code in c.

## slurm stuff

use `sbatch` to submit the run script to compute nodes by slurm. 

there are also other ways to do that, like `srun`, `salloc`, etc.  

## MPI stuff

initialize MPI with `MPI_Init(int** argc, char*** argv)` and finish with `MPI_Finalize()`.

use `MPI_Comm_size(MPI_Comm, int*)` to get the total threads in this **communicator** and use `MPI_Comm_rank(MPI_Comm, int*)` to get the id of this thread in given **communicator**. 

`MPI_Comm` is the type of **communicator**.

the *rank* ranges in $[0, \text{size}-1)$

## make stuff

`-pedantic`: The `-pedantic` option is useful for ensuring that code is written in a portable and standards-compliant way, making it more likely to work correctly on different systems and with different compilers.

`-Werror`: take warings as errors.

`-Wextra`: enable additional set of warning messages.

`-Wall`: enable a set of commonly used warning messages.

```make
%.o: %.c
	$(CC) $(FLAGS) -c $< -o $@

$(EXE): $(OBJ)
	$(CC) $(FLAGS) $^ -o $@
```

`$<` the first dependency oject, `$@` the target, `$^` all dependency objects.

`%.o: %.c` automatically generate the rules.  

> `%.o : %.c` rules doesn't work on my server. 

## others

to compile the source code, we need to first load the necessary software by `module add <package>` to workspace. here the command is `module add mpich`.
