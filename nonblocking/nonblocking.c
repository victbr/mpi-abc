#include <mpi.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char** argv){
    MPI_Init(&argc, &argv); // Initialize the MPI environment
    int a, b, size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size); // Get the number of processes
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); // Get the rank of the process
    MPI_Request request;
    MPI_Status status;
    int flag;
    if (rank == 0){
        a = 1;
        double t0 = MPI_Wtime();
        MPI_Isend(&a, 1, MPI_INT, 1-rank, 0, MPI_COMM_WORLD, &request);
        printf("Process %d sent token %d to process %d asynchronously. \n", rank, 
            a, 1 - rank);
        MPI_Test(&request, &flag, &status);
        printf("Process %d sent status source %d, tag %d, flag %d.\n", rank, 
            status.MPI_SOURCE, status.MPI_TAG, flag);
        MPI_Wait(&request, &status);
        printf("Process %d sent status source %d, tag %d, flag %d.\n", rank, 
            status.MPI_SOURCE, status.MPI_TAG, flag);
        double t1 = MPI_Wtime();
        printf("Process %d takes %f seconds to send token %d to process %d.\n", 
            rank, t1 - t0, a, 1 - rank);
        printf("Process %d is sleeping for 5 seconds\n", rank);
        sleep(5);
        MPI_Irecv(&b, 1, MPI_INT, 1-rank, 0, MPI_COMM_WORLD, &request);
        printf("Process %d received token %d from process %d\n", rank, b, 1 - rank);
        double t2 = MPI_Wtime();
        printf("Process %d takes %f seconds to receive token %d from process %d.\n", 
            rank, t2 - t1, b, 1 - rank);
    }else if (rank == 1){
        a = 2;
        MPI_Status status;
        double t0 = MPI_Wtime();
        printf("Process %d is sleeping for 5 seconds\n", rank);
        sleep(5);
        MPI_Irecv(&b, 1, MPI_INT, 1-rank, 0, MPI_COMM_WORLD, &request);
        printf("Process %d received token %d from process %d\n", rank, b, 1 - rank);
        MPI_Test(&request, &flag, &status);
        printf("Process %d received status source %d, tag %d, flag %d.\n", rank, 
            status.MPI_SOURCE, status.MPI_TAG, flag);
        double t1 = MPI_Wtime();
        printf("Process %d takes %f seconds to receive token %d from process %d.\n", 
            rank, t1 - t0, b, 1 - rank);
        MPI_Isend(&a, 1, MPI_INT, 1-rank, 0, MPI_COMM_WORLD, &request);
        printf("Process %d sent token %d to process %d asynchronously. \n", rank, a, 
            1 - rank);
        MPI_Test(&request, &flag, &status);
        printf("Process %d sent status source %d, tag %d, flag %d.\n", rank, 
            status.MPI_SOURCE, status.MPI_TAG, flag);
        double t2 = MPI_Wtime();
        printf("Process %d takes %f seconds to send token %d to process %d.\n", 
            rank, t2 - t1, a, 1 - rank);
        // MPI_Cancel(&request);
        MPI_Wait(&request, &status);
        double t3 = MPI_Wtime();
        printf("Process %d takes %f seconds to cancel token %d to process %d.\n", 
            rank, t3 - t2, a, 1 - rank);
    }
    MPI_Finalize(); // Finalize the MPI environment.
}