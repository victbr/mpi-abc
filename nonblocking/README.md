# non-blocking

`send` and `recv` with return only after the communication is finished.

non-blocking: commit the request, and return immediately. 

looks like coroutine?

```c
int MPI_Isend(void *buf, int count, MPI_Datatype datatype, int dest, int tag, 
    MPI_Comm comm, MPI_Request *request);
int MPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source, int tag
    MPI_Comm comm, MPI_Request *request);
```

request 用来标记通信任务。

```c
int MPI_Cancel(MPI_Request* request); ///cancel the request.

int MPI_Test(MPI_Request* request, int* flag, MPI_Status* status); //get the status of request.

int MPI_Wait(MPI_Request* request, MPI_Status* status); //wait for request finished.

```

```c
int MPI_Testall(int count, MPI_Request requests[], int* flag, MPI_Status statuses[]);
int MPI_Waitall(int count, MPI_Request requests[], MPI_Status statuses[]);

int MPI_Testany(int count, MPI_Request requests[], int* index, int* flag, MPI_Status* status);
int MPI_Waitany(int count, MPI_Request requests[], int* index ,MPI_Status* status);

// MPI_Testsome / MPI_Waitsome
```

```c
double MPI_Wtime();
double MPI_Wtick(); /// resolution of Timer.

t0 = MPI_Wtime();
// do some works
t1 = MPI_Wtime(); 
```


###### todo

`MPI_Wait` seems didn't wait for `Isend`, it did wait for `Irecv`.

find out the meaning of the values of `flag` and `MPI_Status`.

what does `MPI_Cancel` really do. It looks like just giving up the task? 
